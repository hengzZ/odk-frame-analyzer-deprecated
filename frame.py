from typing import Dict, Any
from datetime import datetime

from pydantic import BaseModel

class RawFrame(BaseModel):
    img: str
    taken_at: datetime
    lat_lng: Dict
    stream_id: str
    
    stream_meta: Dict = {}
    detected_objects: Dict = {}
    object_count: Dict = {}
    img_meta: Dict = {}
    analyser_meta: Dict = {}
    processed_img: Any = None
