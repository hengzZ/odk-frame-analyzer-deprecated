import io
import logging
import os
from datetime import datetime
import random
import cv2
import numpy as np
from imageio import imread
import base64
import torch
import sys

sys.path.append("yolov5")

from utils import torch_utils
from utils.datasets import letterbox
from utils.general import check_img_size, non_max_suppression, scale_coords, plot_one_box


class GarbageImageClassifier:
    
    """
    
    Classification models

    Image to json output with detected objects
    
    """

    def __init__(self):
        curScriptPath = os.path.dirname(os.path.abspath(__file__)) # needed to keep track of the current location of current script ( although it is included somewhere else )

        # Initialize
        self.device = torch_utils.select_device('')
        self.num_classes = 17
        # Load model
        weights = 'weights/garb_weights_l.pt'
        self.model = torch.load(weights, map_location=self.device)['model'].float().eval()
        self.imgsz = check_img_size(640, s=self.model.stride.max())  # check img_size


        self.createLogger()
        self.logger.info("GarbageImageClassifier: Init")
        
    # ----
    
    def createLogger(self):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level=logging.INFO) # SETTING: log level
        
        # logger handlers
        handler = logging.StreamHandler()
        # handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)-4s %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)


    def create_output_json(self, bbox, classes):
        c1, c2 = (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3]))

        label = classes[int(bbox[5])]
        confidence = int(float(bbox[4]) * 100)

        return {
            'detected_object_type': label,
            'confidence': confidence,
            'bbox': {
                'coordinate1': c1,
                'coordinate2': c2
            }
        }

    def detect_image(self, b64_string):

        print('Detecting...')

        # all_images_attributes = []

        conf_thres =0.3
        iou_thres =0.5
        agnostic_nms = True
        augment = False
        img_size= 640

        classes = self.model.module.names if hasattr(self.model, 'module') else self.model.names

        # Run inference
        start_time = datetime.now()

        # reconstruct image as an numpy array

        # remove `data:image/jpeg;base64,` from string
        if b64_string.find(',') > -1:
            b64_clean = b64_string.split(',')[1]
        else:
            b64_clean = b64_string

        original_img = imread(io.BytesIO(base64.b64decode(b64_clean)))

        img = cv2.cvtColor(original_img, cv2.COLOR_BGR2RGB)
        # Padded resize
        img = letterbox(img, new_shape=img_size)[0]

        cv_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2RGB)

        # Convert
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)

        img = torch.from_numpy(img).to(self.device)
        img = img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)


        # Inference
        pred = self.model(img, augment=augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, conf_thres, iou_thres, classes=False, agnostic=agnostic_nms)[0]

        # Return if nothing is detected
        if pred is None:
            return None, None

        images_attributes = {}

        images_attributes['frame_meta'] = {'width': original_img.shape[1],
                                           'height': original_img.shape[0]}


        pred[:, :4] = scale_coords(img.shape[2:], pred[:, :4], original_img.shape).round()
        boxes = []

        for i, det in enumerate(pred): 
            boxes.append(self.create_output_json(det, classes))

        images_attributes['detected_objects'] = boxes

        colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(self.num_classes)]

        # Filter out privacy sensitive objects.
        blur_color = (0, 0, 0)
        for i, box in enumerate(boxes):
            *xyxy, conf, cls = pred[i]
            if 'privacy' in box['detected_object_type']:
                print("Blurrring", box['detected_object_type'])
                cv2.rectangle(cv_img, box['bbox']['coordinate1'], box['bbox']['coordinate2'], blur_color, -1)
            else:
                label = '%s %.2f' % (box['detected_object_type'], conf)
                plot_one_box(xyxy, cv_img, label=label, color=colors[int(cls)], line_thickness=1)

        retval, buffer = cv2.imencode('.jpg', cv_img)


        b64_image = base64.b64encode(buffer)
        b64_image_file = base64.b64decode(b64_image)
        # images_attributes['take_frame'] = {'img':'data:image/jpeg;base64,'+ b64_image.decode("utf-8") }

        images_attributes['counts'] = {x: 0 for x in classes}
        images_attributes['counts']['total'] = 0

        for box in boxes:
            images_attributes['counts'][box['detected_object_type']] += 1
            images_attributes['counts']['total'] += 1
        end_time = datetime.now()

        print('Detection finished in %s' % (end_time - start_time))

        images_attributes['analyser_meta'] = {
            'ml_start_at': str(start_time),
            'ml_done_at': str(end_time),
            'ml_time_taken': str(end_time - start_time)
        }
        return images_attributes, b64_image_file

if __name__ == "__main__":
    classifier = GarbageImageClassifier()
    classifier.detect_image("test_image2.jpg")